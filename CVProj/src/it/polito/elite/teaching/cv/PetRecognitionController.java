package it.polito.elite.teaching.cv;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Queue;
import java.util.Deque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.event.ChangeListener;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import it.polito.elite.teaching.cv.utils.Utils;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;


public class PetRecognitionController
{
	// the FXML button
	@FXML //basta dichiarare chiocciola fxml e java sa come linkare l'oggetto chiamato button con il corrispettivo omonimo nel file fxml
	private Button button;
	// the FXML image view
	@FXML
	private ImageView currentFrame;
	
	@FXML
	private Text title;
	@FXML
	private Label alarm;
	@FXML
	private Text choseString;
	//private Image small=new Image(""); //getClass().getClassLoader().getResourceAsStream("/dog.jpg"));
	@FXML
	private ChoiceBox<String> choiceBox;
	
	
	private static final double INERTIA_THRESHOLD=2E8;
	private enum PetSize{
		Small(3.0f),
		Medium(4.2f),
		Big(5.0f);
		
		private float size;

	     PetSize(float size) {
	         this.size = size;
	     }

	     public float getPetSize() { 
	         return size;
	     }
	}
	
	private PetSize selectedSize=PetSize.Medium;
	
	private ArrayDeque<Mat> framesDeque= new ArrayDeque<Mat>();
	private ArrayDeque<Mat> diffDeque= new ArrayDeque<Mat>();
	private int dequeSize=120;
	private int framesInDeque=0;
	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	// the OpenCV object that realizes the video capture
	private VideoCapture capture = new VideoCapture();
	// a flag to change the button behavior
	private boolean cameraActive = false;
	// the id of the camera to be used
	private static int cameraId = 0; //numeri progressivi attivano le altre camere di default del pc se ci sono
	
	private int surveillanceFrames=120; //
	private int surveilledFrame=0;
	private int addedFromSurveillance=0;
	

	@FXML
	protected void saveSettings(ActionEvent event) {
		
		if(cameraActive) {
			Alert alert = new Alert(AlertType.WARNING, "To edit settings alarm must be deactivated. Deactivate alarm?", ButtonType.YES, ButtonType.CANCEL);
			alert.setTitle("Warning");
			alert.setHeaderText(null);
			alert.showAndWait();
			if (alert.getResult() == ButtonType.YES) {

				this.cameraActive = false;
				this.button.setText("Start Camera");
				this.stopAcquisition();
			}else if(alert.getResult() == ButtonType.CANCEL) {
				return;
			}
		}
		
		String s=this.choiceBox.getSelectionModel().getSelectedItem();
		switch(s){
		case "Small":
			selectedSize=PetSize.Small;
			break;
		case "Medium":
			selectedSize=PetSize.Medium;
			break;
		case "Big":
			selectedSize=PetSize.Big;
			break;
			
			
		}
		Alert alert = new Alert(AlertType.INFORMATION, "Settings successfully updated", ButtonType.OK );
		alert.setTitle("Settings Update");
		alert.setHeaderText(null);
		alert.showAndWait();
		
	}
	
	/**
	 * The action triggered by pushing the button on the GUI
	 *
	 * @param event
	 *            the push button event
	 */
	@FXML
	protected void startCamera(ActionEvent event)
	{
		framesDeque= new ArrayDeque<Mat>();
		alarm.setText("OK");
		alarm.setTextFill(Color.LAWNGREEN);
		if (!this.cameraActive)
		{
			// start the camera capture
			this.capture.open(cameraId);
			//start video capture
			//this.capture.open("C:\\somepath\\somefile.mp4");
			// is the video stream available?
			if (this.capture.isOpened())
			{
				this.cameraActive = true;
				
				// grab a frame every 33 ms (30 frames/sec)
				Runnable frameGrabber = new Runnable() {
					
					@Override
					public void run()
					{
						// effectively grab and process a single frame
						Mat frame = grabFrame();
						// convert and show the frame
						if(frame.width() ==0) {
							Platform.runLater(new Runnable() {
				                 @Override public void run() {
				                	 setClosed();
				                 }
				             });
							
							return;
						}
						Image imageToShow = Utils.mat2Image(frame);
						if(imageToShow.getWidth()==0) {
							System.out.println("was 0");
						}
						updateImageView(currentFrame, imageToShow);
					}
				};
				
				this.timer = Executors.newSingleThreadScheduledExecutor();
				this.timer.scheduleAtFixedRate(frameGrabber, 0, 28, TimeUnit.MILLISECONDS);
				//this.timer.scheduleAtFixedRate(frameGrabber, 0, 3, TimeUnit.MILLISECONDS);
				// update the button content
				this.button.setText("Stop Camera");
			}
			else
			{
				// log the error
				System.err.println("Impossible to open the camera connection...");
			}
		}
		else
		{
			// the camera is not active at this point
			this.cameraActive = false;
			// update again the button content
			this.button.setText("Start Camera");
			
			// stop the timer
			this.stopAcquisition();
		}
	}
	
	/**
	 * Get a frame from the opened video stream (if any)
	 *
	 * @return the {@link Mat} to show
	 */
	
	
	
	
	private Mat grabFrame()
	{
		// init everything
		Mat frame = new Mat();
		Mat difference= new Mat();
		List<Mat> l0= new ArrayList<Mat>();
		
		// check if the capture is open
		if (this.capture.isOpened())
		{
			try
			{
				
					// read the current frame
					this.capture.read(frame);
					
					// if the frame is not empty, process it
					if (!frame.empty())
					{
						
						Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2GRAY);
						//Imgproc.resize(frame, frame, new Size(720,480)); 
						Imgproc.resize(frame, frame, new Size(480,320)); 
						boolean isDifferent=false;

						if(framesDeque.size()>=120) {
							
							//l0.add(frame);
							//Mat histf= new Mat();
							//Imgproc.calcHist(l0, new MatOfInt(0), new Mat(), histf, new MatOfInt(256), new MatOfFloat(0f, 256f));
							//Imgproc.cvtColor(frame, histf, Imgproc.COLOR_BGR2HSV);
							//Mat histoldf= new Mat();
							//l0.clear();
							//l0.add(framesDeque.getFirst());
							//Imgproc.calcHist(l0, new MatOfInt(0), new Mat(), histoldf, new MatOfInt(256), new MatOfFloat(0f, 256f));
							//Double d=Imgproc.compareHist(histf, histoldf, Imgproc.HISTCMP_BHATTACHARYYA); //calcola la differenza di distribuzione tra i due istogrammi
							//System.out.println("hist: "+ d);
							//MatOfInt diff = new MatOfInt(CvType.CV_32S);
							Core.absdiff(frame, framesDeque.getFirst(), difference);
							int sizeR= (int) (difference.rows());
							int sizeC= (int) (difference.cols());
							float maxError= sizeR*sizeC*255;
							float actualError=0;
							float slicedError=0;
							int xcm=0; //x del centro di massa
							int numx=0; //numeratore per trovare xcm
							int denx=0;//denominatore per trovare xcm
							int ycm=0; //y del centro di massa
							int numy=0;//numeratore per trovare ycm
							int deny=0;//denominatore per trovare xcm
							double inerziax=0;
							double inerziay=0; //se l'oggetto ha un inerzia bassa lungo y rispetto a x allora la forma � sviluppata lungo l'asse y, come una persona in piedi
							//int[] temp = new int[sizeR];
							//double[] val= diff.get(0, 0);
							
							//diff.get(0, 0, temp);
							for (int i=0; i<sizeR; i++) {
								
								for(int j=0; j<sizeC; j++) {
									double[] val= difference.get(i, j);
									actualError+=val[0];
									if(val[0]<32) { //bit-plane slicing: will consider only values greater than 32, the others will be set to 0 as their value on the two MSBs are 0
										difference.put(i, j, 0);
										val[0]=0;		
									}else {
										val[0]=1;
										difference.put(i, j, 1);
									}
										
										
									
									slicedError+=val[0];
									numx+=val[0]*j; //columns are the x
									denx+=val[0];
									numy+=val[0]*i; //rows are the y
									deny+=val[0];
									
									
								}
								
							}
							if(denx!=0) {
								xcm=numx/denx;
									
							}
							if(deny!=0) {
								
								ycm=numy/deny;	
							}
							int m=Core.countNonZero(difference); //number of different pixels
							Imgproc.putText(frame, "X", new Point(xcm,ycm), 1, 1, new Scalar(0, 0, 0, 255));
							//System.out.println("Centro di massa dell'errore: "+ xcm+"," +ycm);
							float percError= (actualError/maxError)*100;
							System.out.println("Error:" + percError + "/100");
							float percSlicedError= (slicedError/(sizeR*sizeC))*100 ;
							System.out.println("Sliced error:" +percSlicedError + "/100" );
							//difference.put(0, 0, temp);
							
							//float motionPercent = (nonZeroValue(added)/nonZeroValue(frame)) * 100;
							System.out.println("diff: "+ m);
							//int threatFactor=
							//if(m>50) {						//frame is inserted in the buffer only if it is not a changed frame
								
							if(percSlicedError>selectedSize.size) {
								isDifferent=true;
								if(percSlicedError<selectedSize.size+0.3f) { //for a x% error margin i consider the inertia
									
									for (int i=0; i<sizeR; i++) {										
										for(int j=0; j<sizeC; j++) {
											double[] val= difference.get(i, j);
											if(val[0]!=0) {									
												inerziax+= Math.pow(j-xcm, 2);//columns are the x. la massa � stata omessa in quanto considerata 1 per tutti i pixel
												inerziay+=Math.pow(i-ycm, 2);//rows are the y
											}											
										}										
									}
									
									System.out.println("Inerzia dell'errore: "+ inerziax+"," +inerziay);
									//DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
									//df.setMaximumFractionDigits(8);
									//String s=df.format(inerziay);
									//inerziay=Double.parseDouble(s);
									if(Math.abs(inerziay - inerziax)>INERTIA_THRESHOLD && (inerziax>inerziay)) {
										//alarm goes off
										surveilledFrame=0;
										addedFromSurveillance=0;
										Platform.runLater(new Runnable() {
							                 @Override public void run() {
							                	 alarm.setText("Alarm!");
													alarm.setTextFill(Color.RED);
													System.out.println("----ALARM---");
							                 }
							             });
										
										
									}else {
										//suspect activity but alarm does not go off.. 
										//surveilledFrame is increased and when it comes to 20 and the inertia threshold is still not reached and the extra error is
										//still under +1% then the frame is added to queue
										//when the alarm goes off or error returns under threshold surveilledFrame is reset to 0 
										
										
										if(surveilledFrame==surveillanceFrames) {
											
											if(addedFromSurveillance==framesDeque.size()) {
												surveilledFrame=0;
												addedFromSurveillance=0;
											}else {
												isDifferent=false;
												addedFromSurveillance+=1;
			
												framesDeque.removeFirst();
												System.out.println("surveillancePassed");
											}
											
											
											
										}else {
											surveilledFrame+=1;
										}
									}
									
								}else {
									
									
									if(percSlicedError<selectedSize.size+1.5f) //error due to shadow could be double the size of the dog. i'm assuming shadows won't be too long in an apartment setting with a high camera
									{
										for (int i=0; i<sizeR; i++) {										
											for(int j=0; j<sizeC; j++) {
												double[] val= difference.get(i, j);
												if(val[0]!=0) {									
													inerziax+= Math.pow(j-xcm, 2);//columns are the x. la massa � stata omessa in quanto considerata 1 per tutti i pixel
													inerziay+=Math.pow(i-ycm, 2);//rows are the y
												}											
											}										
										}
										//will check inertia
										System.out.println("Inerzia dell'errore: "+ inerziax+"," +inerziay);
										if(Math.abs(inerziay - inerziax)>INERTIA_THRESHOLD && (inerziax>inerziay) ) {
											//alarm goes off
											surveilledFrame=0;
											addedFromSurveillance=0;
											Platform.runLater(new Runnable() {
								                 @Override public void run() {
								                	 alarm.setText("Alarm!");
														alarm.setTextFill(Color.RED);
														System.out.println("----ALARM---");
								                 }
								             });
									
										}
									}else {
										
										//alarm goes off
										
										surveilledFrame=0;
										addedFromSurveillance=0;
										Platform.runLater(new Runnable() {
							                 @Override public void run() {
							                	 alarm.setText("Alarm!");
													alarm.setTextFill(Color.RED);
													
													System.out.println("----ALARM---");
							                 }
							             });
									}
	
								}	
								
							}else {
								surveilledFrame=0;
								addedFromSurveillance=0;
								framesDeque.removeFirst();
								
							}
						}
						if(!isDifferent) {
							framesDeque.add(frame);
							System.out.println("added"); 
							//Core.absdiff(frame, framesDeque.getLast(), difference);
							//diffDeque.add(difference);
						}	

					}					
				
			}
			catch (Exception e)
			{
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		}
//		if(framesDeque.size()>=120)	
			//return difference;
//		else 
			return frame;
	}
	
	/**
	 * Stop the acquisition from the camera and release all the resources
	 */
	private void stopAcquisition()
	{
		if (this.timer!=null && !this.timer.isShutdown())
		{
			try
			{
				// stop the timer
				this.timer.shutdown();
				this.timer.awaitTermination(33, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException e)
			{
				// log any exception
				System.err.println("Exception in stopping the frame capture, trying to release the camera now... " + e);
			}
		}
		
		if (this.capture.isOpened())
		{
			// release the camera
			this.capture.release();
		}
	}
	
	/**
	 * Update the {@link ImageView} in the JavaFX main thread
	 * 
	 * @param view
	 *            the {@link ImageView} to update
	 * @param image
	 *            the {@link Image} to show
	 */
	private void updateImageView(ImageView view, Image image)
	{
		Utils.onFXThread(view.imageProperty(), image);
	}
	
	/**
	 * On application close, stop the acquisition from the camera
	 */
	protected void setClosed()
	{
		this.cameraActive = false;
		this.button.setText("Start Camera");
		this.stopAcquisition();
	}
	
}
